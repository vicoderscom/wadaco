@extends('layouts.full-width')


@section('banner')

    <div class="banner-no-home">
        <img src="{{ asset2('images/banner-trong.jpg') }}">
    </div>

@endsection


@section('content')

    @while(have_posts())
	
		{!! the_post() !!}

		<div class="container">
			<div class="row">
				<div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12">
	       			 @include('partials.content-single-' . get_post_type())
	    		</div>
    			<?php view('sidebar-single'); ?>
			</div>
		</div>

    @endwhile

@endsection
