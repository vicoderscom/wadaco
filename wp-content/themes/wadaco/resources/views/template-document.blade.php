@extends('layouts.full-width')


@section('banner')

    @php
        $pageId = get_the_ID();
        $banner_img = wp_get_attachment_image_src(get_post_thumbnail_id($pageId), 'full');
        $banner_img_check = $banner_img[0];
    @endphp

    <div class="banner-no-home">

        @if (!empty($banner_img_check))
            <img src="{{ $banner_img_check }}">
        @else
            <img src="{{ asset2('images/banner-trong.jpg') }}">
        @endif

    </div>

@endsection


@section('content')

    @while (have_posts())

        {!! the_post() !!}
     				
			<div class="document">
				<div class="container">
					{{ view('partials.page-header') }}
					<div class="content-document">
						<table class="table-document">
							<tr class="row-document">
								<th class="colvb titlevb">Nơi ra công bố</th>
								<th class="colvb titlevb">Hiệu lực</th>
								<th class="colvb titlevb">Tiêu đề hoặc nội dung tóm lược</th>
							</tr>
							@php
							    $shortcode = '[listing post_type="vanban" layout="partials.content-document" paged="yes" per_page="12"]';
							    echo do_shortcode($shortcode);
							@endphp							
						</table>
					</div>
				</div>
			</div>

    @endwhile

@endsection