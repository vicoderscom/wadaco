@extends('layouts.full-width')


@section('banner')

    @php
        $pageId = get_the_ID();
        $banner_img = wp_get_attachment_image_src(get_post_thumbnail_id($pageId), 'full');
        $banner_img_check = $banner_img[0];
    @endphp

    <div class="banner-no-home">

        @if (!empty($banner_img_check))
            <img src="{{ $banner_img_check }}">
        @else
            <img src="{{ asset2('images/banner-trong.jpg') }}">
        @endif

    </div>

@endsection


@section('content')

    @while(have_posts())

    	{!! the_post() !!}

		<div class="container">
			<div class="row">
				<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12 intro-page">
					{{ view('partials.page-header') }}
					<div class="other-news">
						<div class="content">
							<?php the_content();?>
						</div>
						<div class="other-news-title">
							<h2>Các tin bài khác</h2>
						</div>
						<div class="other-news-content">
							<ul>
								@php
								    $shortcode = '[listing post_type="post" orderby="rand" layout="partials.intro-random" per_page="5" ]';
								    echo do_shortcode($shortcode);
								@endphp
							</ul>
						</div>
					</div>
				</div>
				<?php get_sidebar();?>
			</div>
		</div>

    @endwhile

@endsection
