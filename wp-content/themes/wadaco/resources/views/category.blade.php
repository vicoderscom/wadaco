@extends('layouts.full-width')


@php
    $current_category = get_category_by_slug( get_query_var( 'category_name' ) );
    $cat_id = $current_category->term_id;
    $cat_name = get_cat_name($cat_id);
@endphp

@section('banner')

    @php
        $image_cat = get_field('image_cat','category_'.$cat_id.'');
        $banner_img_check = $image_cat['url'];
    @endphp

    <div class="banner-no-home">

        @if (!empty($banner_img_check))
            <img src="{{ $banner_img_check }}">
        @else
            <img src="{{ asset2('images/banner-trong.jpg') }}">
        @endif

    </div>

@endsection


@section('content')

    <div class="container">
        <div class="row">
            <div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12 theme-5 category-page">
                <h2 class="widget-title">{{ $cat_name }}</h2>
                @php
                    $shortcode = "[listing cat=$cat_id layout='partials.content-category' paged='yes' per_page='5']";
                    echo do_shortcode($shortcode);
                @endphp
            </div>

            <?php get_sidebar();?>
        </div>
    </div>

@endsection
