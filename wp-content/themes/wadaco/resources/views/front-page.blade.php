@extends('layouts.full-width')

@section('content')

    @while (have_posts())

        {!! the_post() !!}

			<div class="banner">
				<div class="banner-slide">
			        @php
			            $anh_banner = get_field('anh_banner');
			            foreach($anh_banner as $img_banner) {
			        @endphp
                    	<img src="{{ asset2('images/2x1.png') }}" style="background-image: url({{ $img_banner['url'] }});" />
			        @php
			            }
			        @endphp
		        </div>
			</div>
			<div class="front-page-content">
				<div class="container">
					<div class="row">
						<div class="col-xl-9 col-lg-9 col-md-9 col-sm-12 col-12 home-content">
							<div class="message">
								<h2 class="widget-title">Thông báo</h2>
							</div>
							<div class="mesage-content">
								<div class="row">
									<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
										@php
											$message = get_field('thong_bao');
											$img_mesage = get_field('anh_thong_bao');
										@endphp
										<img src="{{ $img_mesage['url'] }}" alt="">
									</div>
									<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
										<p>{{ $message }}</p>
									</div>
								</div>
							</div>
							<div class="new_news">
								<h2 class="widget-title">Tin tức mới nhất</h2>
								<div class="news-content">
									<?php dynamic_sidebar('hot_news_home');?>
								</div>
							</div>
							<div class="clearboth"></div>

							<?php dynamic_sidebar('customer_service_home');?>

							<div class="affiliated_units">
								<h2 class="widget-title">Đơn vị trực thuộc</h2>
								<div class="affiliated_units_content">
									<ul class="nav nav-tabs" role="tablist">
									@php
										$query = new WP_Query(array(
											'post_type'=>'donvitructhuoc',
											'orderBy'=>'id',
											'order'=>'ASC'
										));
									@endphp

									@foreach($query->posts as $key => $value)
										<li class="nav-item">
											<a class="nav-link {{ ($key === 0) ? 'active' : '' }}" data-toggle="tab" href="#{{ $value->ID }}" role="tab">{{ get_the_title($value->ID) }}</a>
										</li>
									@endforeach
									</ul>
									<!-- Tab panes -->
									<div class="tab-content">
										@foreach($query->posts as $key => $value)
											<div class="tab-pane {{ ($key === 0) ? 'active' : '' }}" id="{{ $value->ID }}" role="tabpanel">
												<div class="row">
													<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
														{!! $value->post_content !!}
													</div>
													<div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12">
														@php
															echo $map = get_field('ban_do', $value->ID);
														@endphp
													</div>
												</div>
											</div>
										@endforeach
									</div>
								</div>
							</div>
							<div class="clearboth"></div>

							{{ view('partials.news-mention') }}

						</div>
						<?php get_sidebar();?>
					</div>
				</div>
			</div>

    @endwhile

@endsection