@extends('layouts.full-width')

@section('content')

    <div class="page-search">
        <div class="container">
            <div class="page-search-content">
                <div class="row">
                    <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 col-12 theme-5">
                        <h2 class="widget-title">
                            <?php _e('Tìm kiếm','wadaco'); ?>
                        </h2>

                        <h1 class="entry-title">
                            <?php _e('Kết quả tìm kiếm cho từ khoá :','wadaco'); ?>
                            <?php echo '['.$_GET['s'].']'; ?>
                        </h1>

                        @while(have_posts())
                            
                            {!! the_post() !!}
                            
                            {{ view('partials.content-search') }}

                        @endwhile
                        
                        {{ view('partials.pagination') }}
                    </div>
                    <?php get_sidebar();?>
                </div>
            </div>
        </div>
    </div>    
    
@endsection
