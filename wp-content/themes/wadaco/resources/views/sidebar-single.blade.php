    <aside class="col-xl-4 col-lg-4 col-md-4 col-sm-12 col-12 box-sidebar">
			<?php
				if(is_front_page()) {
					echo view('partials.api-search-invoice');
					dynamic_sidebar('sidebar-trang-chu');
					echo view('partials.image-video');
				}
				elseif(is_page_template('template-introduce.php')) {
					dynamic_sidebar('sidebar-gioi-thieu');
		        }
				elseif(is_page_template('template-contact.php')) {
					dynamic_sidebar('sidebar-danh-sach-tin');
		        }		        
				elseif(is_singular()) {
					dynamic_sidebar('sidebar-chi-tiet-tin');
		        }
				else {
					dynamic_sidebar('sidebar-danh-sach-tin');
		        }
			?>
    </aside>



