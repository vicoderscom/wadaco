@php
	$title = get_field('title_list_categories');
	$list_tax = get_field('list_categories');
@endphp

<div class="news-mention">
	 <h2 class="widget-title">{{ $title }}</h2>
	<div class="content">
		<ul class="nav nav-tabs row" role="tablist">
			@php
				$y = 0;
			@endphp
			@foreach($list_tax as $list_tax_kq)
				@php
					$list_tax_id = $list_tax_kq['taxonomy'];
					$list_tax_name = get_cat_name($list_tax_id);
				@endphp
					
					<li class="nav-item col-xl-4 col-lg-4 col-md-4 col-sm-4 col-12">
					    
		               <a class="nav-link {{ ($y === 0) ? 'active' : '' }}" href="#mention{{ $y }}" role="tab" data-toggle="tab">
		               	@php
		               		$img_cat = get_field('image_cat','category_'.$list_tax_id.'');
		               		$img_cat_url = $img_cat['url'];
		               	@endphp
		               	@if(!empty($img_cat_url))
		               		<img src="{{ $img_cat_url }}" alt="">
		               	@endif
		                   {{ $list_tax_name }}
		               </a>
					</li>

				@php
					$y++;
				@endphp
			@endforeach

		</ul>
		<!-- Tab panes -->
		<div class="tab-content">		
			@php
				$i = 0;
			@endphp
			@foreach($list_tax as $list_tax_kq)
				@php
					$list_tax_id = $list_tax_kq['taxonomy'];
					$list_tax_name = get_cat_name($list_tax_id);
				@endphp
		
					<div role="tabpanel" class="tab-pane {{ ($i === 0) ? 'active' : '' }}" id="mention{{ $i }}">
					<div class="read_more_news">
						<a href="{{ get_category_link($list_tax_id) }}" rel="bookmark" class="read-more">
							Xem thêm . . .
						</a>
					</div>
						@php
							$query = new WP_Query(array(
								'post_type'=>'post',
								'cat'=>$list_tax_id,
								'showposts'=>10
							));
						@endphp
						@foreach($query->posts as $key => $value)
							@php
								$date = date("d/m/Y", strtotime($value->post_date));
							@endphp
							<div class="title">
								<a href="{{ the_permalink($value->ID) }}">
									<h3><span>*</span>{!! $date.' - '.$value->post_title !!}</h3>
								</a>
							</div>
						@endforeach
					</div>
				
				@php
					$i++;
				@endphp  
			@endforeach
		</div>	  	
	</div>
</div>
