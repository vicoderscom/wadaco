@php
	$list_sidebar = get_field('list_right_sidebar');
@endphp
<div class="list-sidebar">
	<ul>
		@foreach($list_sidebar as $item)
		<li>
			<a href="$item['link']">
			<img src="{{ $item['image']['url'] }}" alt="">
			{{ $item['title'] }}</a>
		</li>
		@endforeach
	</ul>
</div>