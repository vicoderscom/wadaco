



    <article class="item">
        <figure>
			<a href="{{ $url }}" title="{{ $title }}">
				<img src="{{ asset2('images/3x2.png') }}" alt="{{ $title }}" style="background-image: url({{ getPostImage(get_the_ID(), 'news-related') }});" />
			</a>
        </figure>
        <div class="info">
            <div class="title">
                <a href="{{ $url }}">
                    <h3>{{ $title }}</h3>
                </a>
            </div>
			<div class="date">
				{{ "Ngày ".$publish_date }}
			</div>            
            <div class="desc">
				@php
					if (get_the_excerpt() != '') {
						$excerpt = createExcerptFromContent(get_the_excerpt(), 35);
					} else {
						$excerpt = '';
					}
				@endphp
				{{ $excerpt }}
            </div>
            {{ view('partials.see-more') }}
        </div>
    </article>	



