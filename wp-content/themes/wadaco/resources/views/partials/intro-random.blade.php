
<li>
	<a href="{{ $url }}" title="{{ $title }}">
		<i class="fa fa-angle-double-right" aria-hidden="true"></i> {{ $title }}
	</a>
</li>