<div class="partner">
	<div class="container">
		<div class="partner-content">
			@php
			    $shortcode = '[listing post_type="doitac" layout="partials.content-partner" per_page="-1" ]';
			    echo do_shortcode($shortcode);
			@endphp	

		</div>
	</div>
</div>