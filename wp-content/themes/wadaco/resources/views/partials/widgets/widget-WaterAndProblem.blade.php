

@php
    if($i == 0) {
@endphp
                <article class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 item first">
                    <figure>
                        <a href="{{ $url }}">
                            <img src="{{ getPostImage(get_the_ID()) }}" alt="{{ $title }}" />
                        </a>
                    </figure>
                    <div class="info">
                        <div class="title">
                            <a href="{{ $url }}">
                                <h3>{{ $title }}</h3>
                            </a>
                        </div>
                        <div class="desc">
                            @php
                                if (get_the_excerpt() != '') {
                                    $excerpt = createExcerptFromContent(get_the_excerpt(), 40);
                                } else {
                                    $excerpt = '';
                                }
                            @endphp
                            {{ $excerpt }}
                        </div>
                        <a href="{{ $url }}" class="read-more">Xem thêm</a>
                    </div>
                </article>
@php                
    } else {
@endphp    
                <article class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-6 item">
                    <figure>
                        <a href="{{ $url }}">
                            <img src="<?php echo asset2('images/3x2.png'); ?>" alt="{{ $title }}" style="background-image: url({{ getPostImage(get_the_ID()) }});" />
                        </a>
                    </figure>
                    <div class="info">
                        <div class="title">
                            <a href="{{ $url }}">
                                <h3>{{ $title }}</h3>
                            </a>
                        </div>
                        <div class="desc">
                            @php
                                if (get_the_excerpt() != '') {
                                    $excerpt = createExcerptFromContent(get_the_excerpt(), 12);
                                } else {
                                    $excerpt = '';
                                }
                            @endphp
                            {{ $excerpt }}
                        </div>
                        {{ view('partials.see-more') }}
                    </div>
                </article>
@php    
    }
@endphp

