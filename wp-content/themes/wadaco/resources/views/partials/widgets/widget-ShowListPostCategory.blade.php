@php

	switch ($theme) {
	    case 0 :
	        $layout = 'partials.widgets.widget-ListPostCategory';
	        break;
	    case 1 :
	        $layout = 'partials.widgets.widget-ListPostCategory';
	        break;
	    case 2 :
	        $layout = 'partials.widgets.widget-ListPostCategory';
	        break;
	    case 4 :
	        $layout = 'partials.widgets.widget-ServiceHome';
	        break;
	    case 5 :
	        $layout = 'partials.widgets.widget-WaterAndProblem';
	        break;
	}

	switch ($post_type) {
	    case 00 :
	        $posttype = 'post';
	        break;
	    case 11 :
	        $posttype = 'doitac';
	        break;
	}

	echo '<div class="theme-'.$theme.'">';

		if($theme == 5) { echo '<div class="row">'; }

			$shortcode = '[listing post_type="'.$posttype.'" cat="'.$cate.'" per_page="'.$number_post.'" layout="'.$layout.'"]';
			echo do_shortcode($shortcode);

		if($theme == 5) { echo '</div>'; }

	echo '</div>';

@endphp

