<div class="newslooks"> 
	@php
    	$query = new WP_Query(array('showposts' => $number_post, 'meta_key' => 'post_views_count', 'orderby'=> 'meta_value_num', 'order' => 'DESC'));
	@endphp    

	    @while ($query->have_posts())

    		{!! $query->the_post() !!}
	

	        <article class="item">

				<figure>
					<a href="{{ get_permalink() }}">
						<img src="{{ asset2('images/3x2.png') }}" alt="{{ $title }}" style="background-image: url({{ getPostImage(get_the_ID()) }});" />
					</a>
				</figure>

				<div class="title-widget-post">
					<a href="{{ get_permalink() }}">
				    	<h3>{{ get_the_title() }}</h3>
				    </a>
				</div>

			</article>


		@endwhile
	@php
		wp_reset_postdata();
	@endphp
</div>