<div class="msc-listing">

    <div class="menu-gioi-thieu">
        <ul>
            <?php
                $child_cats = get_term_children( $cate, 'category' );
                foreach ($child_cats as $childs) {
                    $child_cat = get_term_by( 'id', $childs, 'category' );

                    echo '<li><a href="'.esc_url(get_term_link(get_term($child_cat->term_id))).'">'.$child_cat->name.'</a>';
                        echo '<ul class="sub-menu">';
                            $query = new WP_Query(array('cat'=>$child_cat->term_id,'showposts'=>$number_post,'order' => 'DESC','orderby' => 'date'));
                            if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
                            ?>

                                <li>
                                    <a href="<?php the_permalink();?>">
                                        <?php the_title();?>
                                    </a>
                                </li>

                            <?php endwhile; wp_reset_query(); else: echo ''; endif;

                        echo '</ul>';
                    echo '</li>';
                }
            ?>
        </ul>
    </div>

</div>

