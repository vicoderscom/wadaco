@php
	$khach_hang_can_biet = get_option('header_customize_khach_hang_can_biet');
	$khach_hang_can_biet_link = get_option('header_customize_khach_hang_can_biet_link');
	$tra_cuu_tien_nuoc = get_option('header_customize_tra_cuu_tien_nuoc');
	$tra_cuu_tien_nuoc_link = get_option('header_customize_tra_cuu_tien_nuoc_link');
	$thanh_toan_dien_tu = get_option('header_customize_thanh_toan_dien_tu');
	$thanh_toan_dien_tu_link = get_option('header_customize_thanh_toan_dien_tu_link');
	$dong_ho_khach_hang = get_option('header_customize_dong_ho_khach_hang');
	$dong_ho_khach_hang_link = get_option('header_customize_dong_ho_khach_hang_link');
	$thu_dien_tu = get_option('header_customize_thu_dien_tu');
	$thu_dien_tu_link = get_option('header_customize_thu_dien_tu_link');
@endphp
<div class="list-icon">
	<a href="<?php echo $khach_hang_can_biet_link ; ?>">
		<img src="{{ $khach_hang_can_biet }}" alt="Khách hàng cần biết">
	</a>
	<a href="{{ $tra_cuu_tien_nuoc_link }}">
		<img src="{{ $tra_cuu_tien_nuoc }}" alt="Tra cứu tiền nước">
	</a>
	<a href="{{ $thanh_toan_dien_tu_link }}">
		<img src="{{ $thanh_toan_dien_tu }}" alt="Thanh toán điện tử">
	</a>
	<a href="{{ $dong_ho_khach_hang_link }}">
		<img src="{{ $dong_ho_khach_hang }}" alt="Đồng hồ khách hàng">
	</a>
	<a href="mailto:{{ $thu_dien_tu_link }}">
		<img src="{{ $thu_dien_tu }}" alt="Thư điện tử">
	</a>
</div>