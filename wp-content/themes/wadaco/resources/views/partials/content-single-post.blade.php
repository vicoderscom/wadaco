<?php
    $a = wp_get_post_categories(get_the_ID());
    $id_category = $a[0];
    $name_category = get_cat_name( $id_category );
?>
<div>
    <div class="single">
        <div class="single-content">
            <h2 class="widget-title">{{ $name_category }}</h2>
            <div>
                <h1 class="entry-title">{{ get_the_title() }}</h1>
            </div>
            <div class="entry-content">
                {!! wpautop(the_content()) !!}
            </div>
        </div>
        @php setPostViews(get_the_ID()) @endphp
        {{ view('partials.social-bar') }}
    </div>
    {{ view('partials.content-related-post') }}
</div>

