<div class="thu-vien-sidebar">
	  <h1>
	  	@php
	  		$slug_hinhanh = get_category_by_slug('thu-vien-hinh-anh');
	  		$slug_video = get_category_by_slug('thu-vien-video');
	  		$cat_link_image = get_category_link($slug_hinhanh->term_id);
	  		$cat_link_video = get_category_link($slug_video->term_id);
	  	@endphp
	  	{{-- <a href="{{ $cat_link_image }}">{{ $slug_hinhanh->name }}</a> --}}
	</h1>
	<section id="gallery">
        <div class="gallery-item sidebar-style">
            <h2><a href="<?php echo $cat_link_image; ?>">Thư viện ảnh</a></h2>
            <?php
            if(is_active_sidebar('sidebar-image-gallery')) {
				dynamic_sidebar('sidebar-image-gallery');
			}
            ?>
        </div>
        
        <div class="gallery-item sidebar-style">
            <h2><a href="<?php echo $cat_link_video; ?>">Thư viện video</a></h2>
            <?php
            if(is_active_sidebar('sidebar-video-gallery')) {
				dynamic_sidebar('sidebar-video-gallery');
			}
            ?>
        </div>
    </section>
</div>