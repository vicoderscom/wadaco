<aside class="news related-post">
	<div><h2 class="widget-title">Các bài viết liên quan</h2></div>
	<div class="related-content theme-5">
		<div class="row">  
			<?php
				global $post;
				$orig_post = $post;
				$categories = get_the_category($post->ID);
				if ($categories) {
					$category_ids = array();
					foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;

					$args=array(
						'category__in' => $category_ids,
						'post__not_in' => array($post->ID),
						'showposts'=>4, // Số bài viết bạn muốn hiển thị.
						'ignore_sticky_posts'=>1
					);
					$my_query = new wp_query($args);
					if( $my_query->have_posts() ) {
						while ($my_query->have_posts()) {
							$my_query->the_post(); 
							?>
								<article class="col-xl-3 col-lg-3 col-md-3 col-sm-6 col-6 item">
									<figure>
										<a href="<?php the_permalink();?>">
											<img src="{{ asset2('images/3x2.png') }}" alt="{{ $title }}" style="background-image: url({{ getPostImage(get_the_ID(), 'news-related') }});" />
										</a>
									</figure>
									<div class="info">
										<div class="title">
											<a href="<?php the_permalink();?>" rel="bookmark">
												<h3><?php the_title();?></h3>
											</a>
										</div>
										<div class="desc">
											@php
												if (get_the_excerpt() != '') {
												$excerpt = createExcerptFromContent(get_the_excerpt(), 12);
												} else {
												$excerpt = '';
												}
											@endphp
											{{ $excerpt }}
										</div>
										{{ view('partials.see-more') }}
									</div>
								</article>
							<?php
						}
					}
				}
			?>
		</div>
	</div>
</aside>