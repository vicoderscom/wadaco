<article class="item">
    <figure>
        <a href="<?php the_permalink();?>">
            <img src="<?php echo asset2('images/3x2.png'); ?>" alt="<?php the_title(); ?>" style="background-image: url({{ getPostImage(get_the_ID()) }});" />
        </a>
    </figure>
  <header>
    <h2 class="entry-title">
    	<a href="{{ get_permalink() }}">{{ get_the_title() }}</a>
    </h2>
  </header>
</article>
