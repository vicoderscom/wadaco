<header class="header">
	<div class="header-top">
    	<div class="container">


			<div class="header-top-content">
				<div class="logo">
					<a href="<?php echo get_option('home');?>">
						<img src="{{ get_option('header_customize_logo') }}" alt="">
					</a>
					<div class="slogan">
						<span class="slogan_large">{{ get_option('header_customize_slogan_large') }}</span>
						<span class="slogan_small">{{ get_option('header_customize_slogan_small') }}</span>
					</div>
				</div>
				<div class="search-header">
	                <div class="search-box">
	                    <form action="{!! esc_url( home_url( '/' ) ) !!}">
	                        <input type="text" placeholder="" required requiredmsg=" <?php echo 'Bạn vui lòng không để trống ˚¬˚ '; ?>" 
	                        id="search-box" name="s" value="{!! get_search_query() !!}">
	                        <div class="search-icon">
	                            <i class="fa fa-search" aria-hidden="true"></i>
	                        </div>
	                    </form>
	                </div>
				</div>
				<div class="header-right">
					{{ view('partials.list-icon') }}
					{{ view('partials.fast-category') }}
				</div>
			</div>

    	</div>
    </div>

    <nav class="menu">
    	<div class="container">
	        <div class="main-menu">
	            @if (has_nav_menu('main-menu'))
	                {!! wp_nav_menu(['theme_location' => 'main-menu', 'menu_class' => 'menu-primary']) !!}
	            @endif
	        </div>
	        <div class="mobile-menu"></div>

            <div class="search-box">
                <form action="{!! esc_url( home_url( '/' ) ) !!}">
                    <input type="text" placeholder="" required requiredmsg=" <?php echo 'Bạn vui lòng không để trống ˚¬˚ '; ?>" 
                    id="search-box" name="s" value="{!! get_search_query() !!}">
                    <div class="search-icon">
                        <i class="fa fa-search" aria-hidden="true"></i>
                    </div>
                </form>
            </div>

	    </div>
    </nav>
</header>
<div class="left-bar">
	<a class="right-bar-phone" href="tel:{{ get_option('header_customize_hotline_header') }}">
		<i class="fa fa-phone" aria-hidden="true"></i>
	<span>{{ get_option('header_customize_hotline_header') }}; </span>
	</a>
	<a class="right-bar-mail" href="mailto:{{ get_option('header_customize_email_header') }}">
		<span>{{ get_option('header_customize_email_header') }}</span>
	</a>
</div>
<!-- <div class="right-bar">
	<a class="right-bar-mail" href="mailto:{{ get_option('header_customize_email_header') }}">
		<span>{{ get_option('header_customize_email_header') }}</span>
	</a>
	<a class="right-bar-phone" href="tel:{{ get_option('header_customize_hotline_header') }}">
		<span>{{ get_option('header_customize_hotline_header') }}</span>
	</a>
	<a class="back-to-top" href="javascript:void(0)">
		<span>Lên đầu trang</span>
	</a>
</div> -->




