
<div class="map-footer">
    <div class="container">
        <div class="map-content">
            <?php dynamic_sidebar('map-footer'); ?>
        </div>
    </div>
</div>


{{ view('partials.partner') }}

<footer class="footer">
    <div class="footer-top">
    	<div class="container">
    		<div class="row">
    			<div class="col-xl-2 col-lg-2 col-md-6 col-sm-6 col-12 footer-logo">
                    <div class="footer-logo-content">
        				<a href="<?php echo get_option('home');?>">
        					<img src="{{ get_option('footer_customize_footer-logo') }}">
        				</a>
        				<span>
        					{{ get_option('footer_customize_footer-slogan') }}
        				</span>
                    </div>
    			</div>
    			<div class="col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 footer-address">
    				<h2 class="widget-title">{{ get_option('footer_customize_title-address') }}</h2>
    				<div class="footer-address-content">
    					<span class="f-address">
	    						<i class="fa fa-map-marker" aria-hidden="true"></i>
    						<a>
	    						{{ get_option('footer_customize_f-address') }}
	    					</a>
    					</span>
    					<span class="f-phone">
    							<i class="fa fa-phone" aria-hidden="true"></i>
    						<a href="tel:{{ get_option('footer_customize_f-phone') }}">
    							{{ get_option('footer_customize_f-phone') }}
    						</a>
    					</span>
    					<span class="f-email">
    							<i class="fa fa-envelope" aria-hidden="true"></i>
    						<a href="mailto:{{ get_option('footer_customize_f-email') }}">
    							{{ get_option('footer_customize_f-email') }}
    						</a>
    					</span>
    					<span class="f-website">
    							<i class="fa fa-globe" aria-hidden="true"></i>
    						<a href="{{ get_option('footer_customize_f-website') }}">
    							{{ get_option('footer_customize_f-website') }}
    						</a>
    					</span>
    				</div>
    			</div>
    			<div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 footer-category">
                    <?php dynamic_sidebar('dich-vu-khach-hang-footer'); ?>
                </div>
    			<div class="col-xl-3 col-lg-3 col-md-6 col-sm-6 col-12 footer-statistic">
                    <?php dynamic_sidebar('thong-ke-truy-cap'); ?>
                </div>
    		</div>
    	</div>
    </div>
    <div class="footer-bottom">
    	<span>
    		{{ get_option('footer_customize_f-social') }}
    	</span>
    </div>
</footer>






