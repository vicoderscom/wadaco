
@php
	$field = get_field( "noi_ra_cong_bo", get_the_ID() );
	$hieuluc = get_field( "hieu_luc", get_the_ID() );
	$vanbanpl = get_field( "link_tai_lieu", get_the_ID() );
@endphp

<tr class="row-van-ban">
	<td class="colvb">
		<?php echo $field; ?>
	</td>
	<td class="colvb">
		<?php echo $hieuluc; ?>
	</td>
	<td class="colvb">
		<a href="<?php echo $vanbanpl; ?>">
			{{$title}}
		</a>
	</td>
</tr>
