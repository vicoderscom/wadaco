
<article class="item">
    <figure>
        <a href="<?php the_permalink();?>">
            <img src="<?php echo asset2('images/3x2.png'); ?>" alt="<?php the_title(); ?>" style="background-image: url({{ getPostImage(get_the_ID(), 'news') }});" />
        </a>
    </figure>
    <div class="info">
        <div class="title">
            <a href="<?php the_permalink();?>">
                <h3><?php the_title();?></h3>
            </a>
        </div>
        <div class="desc">
			@php
				if (get_the_excerpt() != '') {
					$excerpt = createExcerptFromContent(get_the_excerpt(), 55);
				} else {
					$excerpt = '';
				}
			@endphp
			{{ $excerpt }}
        </div>
        {{ view('partials.see-more') }}
    </div>
</article>
