import 'jquery';
import 'bootstrap';
import slick from "slick-carousel/slick/slick.js";

import meanmenu from 'jquery.meanmenu/jquery.meanmenu.min.js';



$(document).ready(function () {
    var elements = $("input, select");
    for (var i = 0; i < elements.length; i++) {
        elements[i].oninvalid = function(e) {
            e.target.setCustomValidity("");
            if (!e.target.validity.valid) {
                 e.target.setCustomValidity(e.target.getAttribute("requiredmsg"));
            }
        };
        elements[i].oninput = function(e) {
            e.target.setCustomValidity("");
        };
    }
    

    $('.menu-gioi-thieu li').find('.sub-menu').find('li').parent().addClass('tamgiaccha');
    $('.menu-gioi-thieu li').find('.tamgiaccha').parent().addClass('sub-click');
    $('.sub-click').append('<a class="sub-click-show sub-click-add" href="javascript:void(0);">+</a>');
    $('.sub-click').append('<a class="sub-click-show sub-click-del" href="javascript:void(0);">-</a>');

    $('.sub-click-add').click(function(){
        $(this).parent().find('> .sub-menu').slideDown();
        $(this).parent().find('> .sub-menu').parent().find('> .sub-click-add').css('display', 'none');
        $(this).parent().find('> .sub-menu').parent().find('> .sub-click-del').css('display', 'flex');      
    });
    $('.sub-click-del').click(function(){
        $(this).parent().find('> .sub-menu').slideUp();
        $(this).parent().find('> .sub-menu').parent().find('> .sub-click-add').css('display', 'flex');
        $(this).parent().find('> .sub-menu').parent().find('> .sub-click-del').css('display', 'none');      
    });    


    $('.main-menu').meanmenu({
        meanScreenWidth: "1024",
        meanMenuContainer: ".mobile-menu",
    });

    $(".back-to-top").on('click', function() {
        $('html, body').animate({
        scrollTop: $('html, body').offset().top
        }, 1000);
    });

    $('.banner-slide').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        autoplay: true,
        pauseOnHover: true,
        prevArrow: '<span class="a-left control-c prev slick-prev"></span>',
        nextArrow: '<span class="a-right control-c next slick-next"></span>',
    });
    $('.partner-content > div').slick({
        slidesToShow: 7,
        slidesToScroll: 3,
        dots: false,
        autoplay: true,
        pauseOnHover: true,
        prevArrow: '<span class="a-left control-c prev slick-prev"></span>',
        nextArrow: '<span class="a-right control-c next slick-next"></span>',
        responsive: [{
            breakpoint: 767,
            settings: {
                slidesToShow: 5,
                slidesToScroll: 3
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3
            }
        }]
    });

    //api
    // $('.item-api-search-invoice .api-button a').click(function(){
    //     var makh = $('#makh').val();
    //     var sohd = $('#sohd').val();
    //     window.open('http://laphoadon.vacom.vn/NguoiMua/InBanTheHien?nguoiban=0106913627&sohoadon='+sohd+'&makhachhang='+makh);
    // });

    // $('.item-api-search-invoice input').keypress(function(event){
    //     var keycode = (event.keyCode ? event.keyCode : event.which);
    //     if (keycode == '13') {
    //         var makh = $('#makh').val();
    //         var sohd = $('#sohd').val();
    //         window.open('http://laphoadon.vacom.vn/NguoiMua/InBanTheHien?nguoiban=0106913627&sohoadon='+sohd+'&makhachhang='+makh);
    //     }
    // });
    //new-news
    $('.news-content > div').slick({
        slidesToShow: 3,
        slidesToScroll: 3,
        dots: false,
        autoplay: false,
        pauseOnHover: true,
        arrows: true,
        prevArrow: '<span class="a-left control-c prev slick-prev"></span>',
        nextArrow: '<span class="a-right control-c next slick-next"></span>',
        responsive: [{
            breakpoint: 767,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });
    $('.thu-vien > div').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        dots: false,
        autoplay: false,
        pauseOnHover: false,
        arrows: true,
        prevArrow: '<span class="a-left control-c prev slick-prev"></span>',
        nextArrow: '<span class="a-right control-c next slick-next"></span>',
        responsive: [{
            breakpoint: 767,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }, {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
        }]
    });
    $('.don-vi-truc-thuoc-content a').click(function (e) {
      e.preventDefault()
      $(this).tab('show')
    })
});

