<?php
/**
 * Enqueue scripts and stylesheet
 */
add_action('wp_enqueue_scripts', 'theme_enqueue_scripts');
add_action('wp_enqueue_scripts', 'theme_enqueue_style');

/**
 * Theme support
 */
add_theme_support('post-thumbnails');

function theme_enqueue_style()
{
    wp_enqueue_style(
        'template-style',
        asset('app.css'),
        false
    );
}

function theme_enqueue_scripts()
{
    wp_enqueue_script(
        'template-scripts',
        asset('app.js'),
        'jquery',
        '1.0',
        true
    );

    $protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';

    $params = array(
       'ajax_url' => admin_url('admin-ajax.php', $protocol)
    );

    wp_localize_script('template-scripts', 'ajax_obj', $params);
}

if (!function_exists('themeSetup')) {
    /**
     * setup support for theme
     *
     * @return void
     */
    function themeSetup()
    {
        // Register menus
        register_nav_menus( array(
    		'main-menu' => __('Main Menu', 'wadaco')
    	) );

        // add_theme_support('menus');
        add_theme_support('post-thumbnails');
        add_image_size('news', 158, 94, true);
        add_image_size('news-related', 193, 121, true);
        add_image_size('news-sidebar', 108, 60, true);
    }

    add_action('after_setup_theme', 'themeSetup');
}

if (!function_exists('themeSidebars')) {
    /**
     * register sidebar for theme
     *
     * @return void
     */
    function themeSidebars()
    {
        $sidebars = [
            [
                'name'          => __('Map Footer', 'wadaco'),
                'id'            => 'map-footer',
                'description'   => __('Map footer', 'wadaco'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],            
            [
                'name'          => __('Dịch Vụ Khách Hàng Footer', 'wadaco'),
                'id'            => 'dich-vu-khach-hang-footer',
                'description'   => __('Dich vu khach hang footer', 'wadaco'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Thống Kê Truy Cập', 'wadaco'),
                'id'            => 'thong-ke-truy-cap',
                'description'   => __('Thống kê truy cập', 'wadaco'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Tin Nổi Bật Trang Chủ', 'wadaco'),
                'id'            => 'hot_news_home',
                'description'   => __('Tin nổi bật trang chủ', 'wadaco'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Dịch Vụ Khách Hàng Trang Chủ', 'wadaco'),
                'id'            => 'customer_service_home',
                'description'   => __('Dịch vụ khách hàng trang chủ', 'wadaco'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Nước Sạch Và Vấn Đề Liên Quan', 'wadaco'),
                'id'            => 'nuoc-sach-va-van-de-lien-quan',
                'description'   => __('Nước sạch và vấn đề liên quan', 'wadaco'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Sidebar Trang Chủ', 'wadaco'),
                'id'            => 'sidebar-trang-chu',
                'description'   => __('Sidebar trang chủ', 'wadaco'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Sidebar Trang Giới Thiệu', 'wadaco'),
                'id'            => 'sidebar-gioi-thieu',
                'description'   => __('Sidebar trang giới thiệu', 'wadaco'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Sidebar Trang Danh Sách Tin', 'wadaco'),
                'id'            => 'sidebar-danh-sach-tin',
                'description'   => __('Sidebar trang danh sách tin và liên hệ', 'wadaco'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Sidebar Trang Chi Tiết Tin', 'wadaco'),
                'id'            => 'sidebar-chi-tiet-tin',
                'description'   => __('Sidebar trang chi tiết tin', 'wadaco'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
            [
                'name'          => __('Sidebar Chuyên mục xem nhanh', 'wadaco'),
                'id'            => 'sidebar-fast-category',
                'description'   => __('Sidebar Chuyên mục xem nhanh tại header', 'wadaco'),
                'before_widget' => '<section id="%1$s" class="widget %2$s">',
                'after_widget'  => '</section>',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ],
        ];

        foreach ($sidebars as $sidebar) {
            register_sidebar($sidebar);
        }
    }

    add_action('widgets_init', 'themeSidebars');
}

if (!function_exists('registerCustomizeFields')) {
    function registerCustomizeFields()
    {
        $data = [
            [
                'info' => [
                    'name' => 'header_customize',
                    'label' => 'Header',
                    'description' => '',
                    'priority' => 1,
                ],
                'fields' => [
                    [
                        'name' => 'logo',
                        'type' => 'upload',
                        'default' => '',
                        'label' => 'Logo'
                    ],
                    [
                        'name' => 'slogan_large',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'Slogan_large'
                    ],
                    [
                        'name' => 'slogan_small',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'Slogan_small'
                    ],
                    [
                        'name' => 'email_header',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'Email_header'
                    ],
                    [
                        'name' => 'hotline_header',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'Hotline_header'
                    ],
                    [
                        'name' => 'khach_hang_can_biet',
                        'type' => 'upload',
                        'default' => '',
                        'label' => 'Ảnh header 1'
                    ],
                    [
                        'name' => 'khach_hang_can_biet_link',
                        'type' => 'Text',
                        'default' => '',
                        'label' => 'Link'
                    ],
                    [
                        'name' => 'tra_cuu_tien_nuoc',
                        'type' => 'upload',
                        'default' => '',
                        'label' => 'Ảnh header 2'
                    ],
                    [
                        'name' => 'tra_cuu_tien_nuoc_link',
                        'type' => 'Text',
                        'default' => '',
                        'label' => 'Link'
                    ],
                    [
                        'name' => 'thanh_toan_dien_tu',
                        'type' => 'upload',
                        'default' => '',
                        'label' => 'Ảnh header 3'
                    ],
                    [
                        'name' => 'thanh_toan_dien_tu_link',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'Link'
                    ],                    
                    [
                        'name' => 'dong_ho_khach_hang',
                        'type' => 'upload',
                        'default' => '',
                        'label' => 'Ảnh header 4'
                    ],
                    [
                        'name' => 'dong_ho_khach_hang_link',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'Link'
                    ],
                    [
                        'name' => 'thu_dien_tu',
                        'type' => 'upload',
                        'default' => '',
                        'label' => 'Ảnh header 5'
                    ],
                    [
                        'name' => 'thu_dien_tu_link',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'Link'
                    ],
                ],              
            ],
            [
                'info' => [
                    'name' => 'footer_customize',
                    'label' => 'Footer',
                    'description' => '',
                    'priority' => 2,
                ],
                'fields' => [
                    [
                        'name' => 'footer-logo',
                        'type' => 'upload',
                        'default' => '',
                        'label' => 'Footer-logo'
                    ],
                    [
                        'name' => 'footer-slogan',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'Footer-slogan'
                    ],
                    [
                        'name' => 'title-address',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'Title-address'
                    ],
                    [
                        'name' => 'f-address',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'F-address'
                    ],
                    [
                        'name' => 'f-phone',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'F-phone'
                    ],
                    [
                        'name' => 'f-email',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'F-email'
                    ],
                    [
                        'name' => 'f-website',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'F-website'
                    ],
                    [
                        'name' => 'f-social',
                        'type' => 'text',
                        'default' => '',
                        'label' => 'F-social'
                    ],
                ],              
            ],            
        ];

        $customizer = new MSC\Customizer\Customizer($data);
        $customizer->create();
    }

    add_action('init', 'registerCustomizeFields');
}


