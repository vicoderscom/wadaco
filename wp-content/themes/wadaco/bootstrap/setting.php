<?php
/**
 *
 */
class Setting
{
    public function __construct()
    {
        // add_image_size( 'category-thumb', 89, 59, true);
        add_action('widgets_init', [$this,'registerWidget']);
    }

    public function registerWidget()
    {
        register_sidebar([
            'name'          => esc_html__('Image Gallery carousel', 'viwaco'),
            'id'            => 'sidebar-image-gallery',
            'description'   => esc_html__('Add widgets here.', 'viwaco'),
            'before_widget' => '',
            'after_widget'  => '',
            'before_title'  => '<h2>',
            'after_title'   => '</h2>',
        ]);

        register_sidebar([
            'name'          => esc_html__('Video Gallery carousel', 'viwaco'),
            'id'            => 'sidebar-video-gallery',
            'description'   => esc_html__('Add widgets here.', 'viwaco'),
            'before_widget' => '',
            'after_widget'  => '',
            'before_title'  => '<h2>',
            'after_title'   => '</h2>',
        ]);
    }
}

new Setting();
