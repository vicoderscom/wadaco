<?php

namespace App\Widgets;

use MSC\Widget;

/**
 *
 */
class NewsLooks extends Widget
{
    public function __construct()
    {
        $widget = [
            'id'          => __('NewsLooks', 'wadaco'),
            'label'       => __('Sidebar tin đọc nhiều', 'wadaco'),
            'description' => __('Sidebar tin đọc nhiều', 'wadaco'),
        ];

        $fields = [
            [
                'label' => __('Số bài hiển thị', 'wadaco'),
                'name'  => 'number_post',
                'type'  => 'number',
            ],
        ];
        parent::__construct($widget, $fields);
    }  

    public function handle($instance)
    {
        $data = [
            'number_post' => $instance['number_post']
        ];

        view('partials.widgets.widget-NewsLooks', $data);
                            
	}
}
