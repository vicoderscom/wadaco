<?php

namespace App\Widgets;

use MSC\Widget;

/**
 *
 */
class ListPostCategory extends Widget
{
    public function __construct()
    {
        $widget = [
            'id'          => __('ListPostCategory', 'wadaco'),
            'label'       => __('Widget đọc bài viết', 'wadaco'),
            'description' => __('Widget đọc bài viết', 'wadaco'),
        ];

        $fields = [
            [
                'label' => __('Số bài hiển thị', 'wadaco'),
                'name'  => 'number_post',
                'type'  => 'number',
            ],
            [
                'label' => __('Chọn loại posttype', 'wadaco'),
                'name'  => 'post_type',
                'type'  => 'select',
                'options' => [
                    '00' => 'post',
                    '11' => 'doitac'
                ]
            ],            
            [
                'label' => __('Chọn category', 'wadaco'),
                'name'  => 'id_category',
                'type'  => 'select',
                'options' => $this->getCategories(),
            ],
            [
                'label' => __('Chọn giao diện', 'wadaco'),
                'name'  => 'theme',
                'type'  => 'select',
                'options' => [
                    '0' => 'Hướng dẫn',
                    '1' => 'Hỏi đáp',
                    '2' => 'Tin nổi bật',
                    '4' => 'Dịch vụ trang chủ',
                    '5' => 'Nước sạch và vấn đề liên quan',
                ]
            ],
        ];
        parent::__construct($widget, $fields);
    }

    public function getCategories()
    {
        $categories = get_categories(array(
            'orderby' => 'name',
            'hide_empty' => false
        ));

        $cates[0] = __('Chọn Category', 'wadaco');

        foreach ($categories as $key => $value) {
            $cates[$value->term_id] = $value->name;
        }

        return $cates;
    }  

    public function handle($instance)
    {
        $data = [
            'post_type' => $instance['post_type'],
            'cate' => $instance['id_category'],
            'number_post' => $instance['number_post'],
            'theme' => $instance['theme'],
        ];

        view('partials.widgets.widget-ShowListPostCategory', $data);
                            
    }
}
