<?php

namespace App\Widgets;

use MSC\Widget;

/**
 *
 */
class Introduce extends Widget
{
    public function __construct()
    {
        $widget = [
            'id'          => __('introduce', 'wadaco'),
            'label'       => __('Sidebar giới thiệu', 'wadaco'),
            'description' => __('Sidebar giới thiệu', 'wadaco'),
        ];

        $fields = [
            [
                'label' => __('Số bài hiển thị', 'wadaco'),
                'name'  => 'number_post',
                'type'  => 'number',
            ],
            [
                'label' => __('Chọn category', 'wadaco'),
                'name'  => 'id_category',
                'type'  => 'select',
                'options' => $this->getCategories()
            ],
        ];
        parent::__construct($widget, $fields);
    }

    public function getCategories()
    {
        $categories = get_categories(array(
            'orderby' => 'name',
            'hide_empty' => false
        ));

        $cates[0] = __('Chọn Category', 'wadaco');

        foreach ($categories as $key => $value) {
            $cates[$value->term_id] = $value->name;
        }

        return $cates;
    }    

    public function handle($instance)
    {
        $data = [
            'cate' => $instance['id_category'],
            'number_post' => $instance['number_post']
        ];

        view('partials.widgets.widget-Introduce', $data);
                            
	}
}
