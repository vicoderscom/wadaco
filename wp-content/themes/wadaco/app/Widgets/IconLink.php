<?php

namespace App\Widgets;

use MSC\Widget;

/**
 *
 */
class IconLink extends Widget
{
    public function __construct()
    {
        $widget = [
            'id'          => __('icon_link', 'wadaco'),
            'label'       => __('Icon Link', 'wadaco'),
            'description' => __('Add icon anh link in right sidebar', 'wadaco'),
        ];

        $fields = [
            [
                'label' => __('Icon', 'wadaco'),
                'name'  => 'icon',
                'type'  => 'text',
            ],
            [
                'label' => __('Title', 'wadaco'),
                'name'  => 'title',
                'type'  => 'text',
            ],
            [
                'label' => __('Link', 'wadaco'),
                'name'  => 'link',
                'type'  => 'text',
            ],
        ];
        parent::__construct($widget, $fields);
    }

    public function handle($instance)
    {
        $data = [
            'link' => $instance['link'],
            'icon' => $instance['icon'],
            'title' => $instance['title'],
        ];
        view('partials.widgets.widget-RightSidebar', $data);
	}
} 
