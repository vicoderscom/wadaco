<?php
/**
 * Sample class for a custom post type
 *
 */

namespace App\CustomPosts;

use NF\Abstracts\CustomPost;

class DoiTac extends CustomPost
{
    /**
     * [$type description]
     * @var string
     */
    public $type = 'doitac';

    /**
     * [$single description]
     * @var string
     */
    public $single = 'DoiTac';

    /**
     * [$plural description]
     * @var string
     */
    public $plural = 'DoiTac';

    /**
     * $args optional
     * @var array
     */
    public $args = ['menu_icon' => 'dashicons-art'];

}
