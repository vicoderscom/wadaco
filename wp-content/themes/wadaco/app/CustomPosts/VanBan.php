<?php
/**
 * Sample class for a custom post type
 *
 */

namespace App\CustomPosts;

use NF\Abstracts\CustomPost;

class VanBan extends CustomPost
{
    /**
     * [$type description]
     * @var string
     */
    public $type = 'vanban';

    /**
     * [$single description]
     * @var string
     */
    public $single = 'VanBan';

    /**
     * [$plural description]
     * @var string
     */
    public $plural = 'VanBan';

    /**
     * $args optional
     * @var array
     */
    public $args = ['menu_icon' => 'dashicons-art'];

}
