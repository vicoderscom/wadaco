<?php

$app = require_once __DIR__ . '/bootstrap/app.php';

include_once get_template_directory(). '/app/Widgets/wp-statistics.php';

add_action('pre_get_posts', function ($query) {
    if ($query->is_search) {
        $query->set('posts_per_page', 5);
    }
});
